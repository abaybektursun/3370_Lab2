# When running script pass argument through command line in this format: lab13.pl [temperature] [RH]
use strict;
use warnings;
 
my ($temperature, $RH) = @ARGV;
 
if (not defined $temperature) {
  die "Need current temperature\n";
}

if (not defined $RH) {
  die "Need RH(%)\n";
}

if($temperature < -50 || $temperature > 150)
{
	die "Invalid temperature"; 
}

if($RH < 0 || $RH > 100)
{
	die "Invalid RH(%)"; 
}

print "RH(%): $RH, Temperature: $temperature";