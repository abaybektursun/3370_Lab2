my @best_food_ever = ("Chirashi", "Zaru Soba", "Nabeyaki Udon", "Spicy Seafood Ramen", "Sashimi", "Sushi");
my @not_so_much = ("Cinnamon", "Cinnamon2","Cinnamon3", "Cinnamon4","Cinnamon5", "Cinnamon6");

print "Some of the food I love: ";
print "$best_food_ever[0], $best_food_ever[1], $best_food_ever[2]\n";

print "Before Step 4: \n";
print join("\n",@not_so_much),"\n";

$last_element   = pop @not_so_much;
$not_so_much[0] = $last_element;

print "After Step 4:\n";
print join("\n",@not_so_much),"\n";

$not_so_much_size    = $#not_so_much;
$best_food_ever_size = $#best_food_ever;
print "The First and Last Elements of best_food_ever:\n";
print "$best_food_ever[0], $best_food_ever[$best_food_ever_size]\n";
print "The First and Last Elements of not_so_much:\n";
print "$not_so_much[0], $not_so_much[$best_food_ever_size]\n";

print "Add food you dislike: ";
$fdislike = <>;

push (@not_so_much, $fdislike);
$not_so_much_size = $#not_so_much;
#print "$not_so_much_size";
print "Last two elements of not_so_much: ";
print "$not_so_much[$not_so_much_size],$not_so_much[$not_so_much_size - 1]";